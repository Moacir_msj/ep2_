EP2-OO (UnB-Gama 2/2018)


Este projeto consiste em criar algumas caracteristicas de uma Pokedex desenvolvida em Java.


INSTRUÇÕES DE DESENVOLVIMENTO:


    O programa foi criado ultilizando a IDE Netbeans 8.2, pelo sistema operacional Ubuntu;
    
    
INSTRUÇÕES DE USO:


1.Ao digitar um texto, em um campo de Texto, precione a tecla enter e clique no botão desejado.

2.Após buscar o pokemon pelo seu nome, aparecerá suas informações e ele poderá ser adicionados aos seus pokemons clicando no botão adicionar.

3.Ao clicar no botão Meus pokemons todos pokemons salvos serão listados.


DADOS DO ALUNO:


Nome: Moacir M.Soares Junior

Matricula: 17/0080366

Repositorio: https://gitlab.com/Moacir_msj/ep2_

