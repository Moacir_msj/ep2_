
package pokedex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class Pokedex {
    
    public Pokedex(){}

    public boolean entrar(String nome ,String senha , Treinador treinador) {
	   		
	    try {
	      FileReader arq = new FileReader("./arquivos/Usuario.txt");
	      BufferedReader lerArq = new BufferedReader(arq);
              String nomeAr ;
              String senhaAr;
              
	     //verificando se existe o treinador
              while(lerArq.ready()){
                    nomeAr = lerArq.readLine();
                    senhaAr = lerArq.readLine();
                   //caso encontre...
                   if((nomeAr == null ? nome == null : nomeAr.equals(nome)) && (senhaAr == null ? senha == null : senhaAr.equals(senha)) ){
                      treinador.setNome(nome);
                      treinador.setSenha(senha);                      
                       return true;                   
                   }
              }
	    } catch (IOException e) {
	        System.err.printf("Erro na abertura do arquivo: %s.\n",
	          e.getMessage());
	        return false;
	    }
		
	return false;
	  }
    
//cadastra o treinador salvando seus daddos em um arquivo    
    public boolean cadastrar(String nome,String senha )throws IOException{
 
        int aux;
        File arquivo = new File("./arquivos/Usuario.txt");
        if(arquivo.exists()){
          }else{
        arquivo.createNewFile();
        }        

//verificando se existe um treinador com nome igual, se existir aux =1
        try (FileReader arq = new FileReader("./arquivos/Usuario.txt")) {
            BufferedReader lerArq = new BufferedReader(arq);
            String nome1,senha1;
            aux = 0;
            while(lerArq.ready()){
                nome1 = lerArq.readLine();
                senha1 = lerArq.readLine();
                if(nome1 == null ? nome == null : nome1.equals(nome)){
                    aux += 1;           
                 return false;
                }
            }
            lerArq.close();
            arq.close();
        }
        //caso não tenha ninguem com o mesmo nome faça o cadastro
         if(aux ==0){
               
            try (FileWriter arqv = new FileWriter("./arquivos/Usuario.txt",true)) {
                PrintWriter gravarArqv = new PrintWriter(arqv);
                gravarArqv.printf(nome+"\r\n");
                gravarArqv.printf(senha+"\r\n");   
            }
       
            String arqDoTreinador;
            arqDoTreinador = "./arquivos/"+nome+".txt";
            File treinadorArq = new File(arqDoTreinador);
            treinadorArq.createNewFile();
	    System.out.printf("Cadastro realizado com sucesso");
                    
            return true;
         }
        
     return false;
	
}
//busca nos arquivos CSV e retorna os atributos do pokemon    
public void buscarPokemon(Pokemon pokemon,String nomePokemon) throws FileNotFoundException, IOException{
    
    nomePokemon.toLowerCase();
     String arqvPokemons= "./arquivos/data_csv_files_POKEMONS_DATA_2.csv";
     String separador = ",";
     BufferedReader conteudo = null;
     String linha ;
     conteudo = new BufferedReader(new FileReader(arqvPokemons) );
     String[] pokemonn;
     String id,name,experience,height,weight,habilidade1,habilidade2,habilidade3,mov1,mov2,mov3,mov4,mov5,mov6,mov7;
     int aux = 0;
     
     while((linha = conteudo.readLine())!=null){
        pokemonn = linha.split(separador);
        id = pokemonn[0];
        name = pokemonn[1];
        experience = pokemonn[2];
        height = pokemonn[3];
        weight = pokemonn[4];
        habilidade1 = pokemonn[5];
        habilidade2 = pokemonn[6];
        habilidade3 =pokemonn[7];
        mov1 = pokemonn[8];
        mov2 = pokemonn[9];
        mov3 =pokemonn[10];
        mov4 = pokemonn[11];
        mov5 = pokemonn[12];
        mov6 = pokemonn[13];
        mov7 = pokemonn[14];
  
        if(name == null ? nomePokemon == null : name.equals(nomePokemon)){
    
           String habilidades,movimentos;
           habilidades = habilidade1+" "+habilidade2+" "+habilidade3;
           movimentos = mov1 +" "+mov2 +" "+mov3 +" "+mov4 +" "+mov5 +" "+mov6 +" "+mov7 ; 
            pokemon.setNome(name);
            pokemon.setHabilidade(habilidades);
            pokemon.setMovimentos(movimentos);
        }       
    }

     conteudo.close();
     arqvPokemons= "./arquivos/data_csv_files_POKEMONS_DATA_1.csv";
     BufferedReader conteudo1 = null;
     linha="";
     String tipos,lendario;
     conteudo1 = new BufferedReader(new FileReader(arqvPokemons) );
      while((linha = conteudo1.readLine())!=null){
        pokemonn = linha.split(separador);
        name= pokemonn[1];
        tipos = pokemonn[2]+"-"+pokemonn[3];
        lendario = pokemonn[12];
        
        if(name.toLowerCase().equals(nomePokemon)){   
            pokemon.setTipo(tipos);
            pokemon.setLendario(lendario);
        } 
      }
     conteudo1.close();
}
//              POLIMORFISMO SOBRECARGA...
//busca nos arquivos csv pelo tipo e retorna os pokemons desse tipo 
 public void buscarPokemon(String tipoPokemon,Comum dados) throws FileNotFoundException, IOException{
      String resultado= "";
     String arqvPokemon= "./arquivos/data_csv_files_POKEMONS_DATA_1.csv";
     String separador = ",";
     BufferedReader conteudo = null;
     String linha ,tipo;
     conteudo = new BufferedReader(new FileReader(arqvPokemon) );
     String[] pokemonn;
     String name,tipo2;
     
     while((linha = conteudo.readLine())!=null){
        pokemonn = linha.split(separador);
        name = pokemonn[1];
        tipo = pokemonn[2]; 
        tipo2 = pokemonn[3];
        if(tipo.toLowerCase() == null ? tipoPokemon.toLowerCase() == null : tipo.toLowerCase().equals(tipoPokemon.toLowerCase()) || (tipo2.toLowerCase() == null ? tipoPokemon.toLowerCase() == null : tipo2.toLowerCase().equals(tipoPokemon.toLowerCase())) ){
        Pokemon pokemon = new Pokemon();
        this.buscarPokemon(pokemon,name.toLowerCase());
        resultado += "Nome: "+pokemon.getNome()+" Tipo(s): "+ pokemon.getTipo()+" Habilidades: "+pokemon.getHabilidade()+" Movimentos: "+pokemon.getMovimentos()+" Lendario: "+pokemon.getLendario()+"\r\n";
        }
    }     
     conteudo.close();
    dados.setNome(resultado);
    }
    
}
