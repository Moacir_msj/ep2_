/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex;

    import java.io.BufferedReader;
    import java.io.File;
    import java.io.FileNotFoundException;
    import java.io.FileReader;
    import java.io.FileWriter;
    import java.io.IOException;
    import java.io.PrintWriter;
import java.util.ArrayList;
    import java.util.Arrays;
    import java.util.List;

    /**
     *
     * @author Moacir
     */
    public class Treinador extends Comum{
           
            private String senha;
            public List <Pokemon> pokemons;

            public Treinador() {
                   pokemons = new ArrayList<Pokemon>();
            }

            public String getSenha() {
                    return senha;
            }

            public void setSenha(String senha) {
                    this.senha = senha;
            }

//busca os pokemons do arquivo e cria objetos com os atributos
     public void carregarPokemons(Treinador treinador, int aux) throws FileNotFoundException, IOException{
         
         if(aux == 0){
         System.out.print("\r\n entrou pokemons");
         
         String nomeArqv = "./arquivos/"+treinador.getNome()+".txt";
         System.out.print(nomeArqv);
         String separador = ",";
         BufferedReader conteudo = null;
         String linha ;
         String[] pokem;
         conteudo = new BufferedReader(new FileReader(nomeArqv));
         while((linha = conteudo.readLine())!=null){
             Pokemon pokemon = new Pokemon();
             pokem= linha.split(separador);
             pokemon.setNome(pokem[0]);
             pokemon.setTipo(pokem[1]);
             pokemon.setHabilidade(pokem[2]);
             pokemon.setMovimentos(pokem[3]);
             pokemon.setLendario(pokem[4]);
             //arquivo ordem dos dados nome,tipo,habilidades,movimentos,lendario
            treinador.pokemons.add(pokemon);
             
         }
         conteudo.close();
         }
         else{}
     }
//adicona o pokemons ao arquivo
     public void adicionarPokemon(Treinador treinador,Pokemon pokemon) throws IOException{
        
         
         String arquivoTreinador = "./arquivos/"+treinador.getNome()+".txt";
         FileWriter arqv = new FileWriter(arquivoTreinador,true);
         PrintWriter gravarArqv = new PrintWriter(arqv);
         gravarArqv.printf(pokemon.getNome()+",");
         gravarArqv.printf(pokemon.getTipo()+",");
         gravarArqv.printf(pokemon.getHabilidade()+",");
         gravarArqv.printf(pokemon.getMovimentos()+",");
         gravarArqv.printf(pokemon.getLendario()+"\r\n");
         
         arqv.close();
         gravarArqv.close();
     }
//pokemons existentes do treinador     
     public String meusPokemons(Treinador treinador){
         String pokemons = "";
         for(Pokemon mov : treinador.pokemons){
          pokemons+="Nome: "+mov.getNome() +" Tipos 1-2: "+mov.getTipo()+" Habilidades: "+mov.getHabilidade()+" Movimentos: " + mov.getMovimentos()+" Legendario: "+ mov.getLendario()+"\r\n";
         }
         return pokemons;
     }

};
