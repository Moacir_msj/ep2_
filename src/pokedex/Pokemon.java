/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex;

/**
 *
 * @author Moacir
 */
public class Pokemon extends Comum {
    
    private String tipo;
    private  String habilidade;
    private String movimentos;
    private String lendario;
    
    public Pokemon() {};
    /**
     * @return the habilidade
     */
    public String getHabilidade() {
        return habilidade;
    }

    /**
     * @param habilidade the habilidade to set
     */
    public void setHabilidade(String habilidade) {
        this.habilidade = habilidade;
    }

    /**
     * @return the movimentos
     */
    public String getMovimentos() {
        return movimentos;
    }

    /**
     * @param movimentos the movimentos to set
     */
    public void setMovimentos(String movimentos) {
        this.movimentos = movimentos;
    }
    public String getTipo() {
            return tipo;
    }
    public void setTipo(String tipo) {
            this.tipo = tipo;
    }
    
    /**
     * @return the lendario
     */
    public String getLendario() {
        return lendario;
    }

    /**
     * @param lendario the lendario to set
     */
    public void setLendario(String lendario) {
        this.lendario = lendario;
    }
  
    
	
	
	
   
}
